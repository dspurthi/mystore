from re import T
from django.db import models

# Create your models here.
class Users(models.Model):
    user_id = models.CharField(max_length=100,unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100,unique=True)
    phone = models.CharField(max_length=100,unique=True)
    address = models.CharField(max_length=100)
    
    class Goal:
        managed = True
        db_table = 'store\".\"users'
    def __str__(self):
        return self.first_name

class Products(models.Model):
    product_id = models.CharField(max_length=100,unique=True)
    product_name = models.CharField(max_length=70)
    brand = models.CharField(max_length=20)
    category = models.CharField(max_length=30)
    price = models.PositiveIntegerField()
    colour = models.CharField(max_length=30)

    class Goal:
        managed = True
        db_table = 'store\".\"products'

    def __str__(self):
        return self.product_name

class Sales(models.Model):
    sale_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Users, db_column="user_id",on_delete=models.CASCADE)
    product = models.ForeignKey(Products,blank=True, db_column="product_id",on_delete=models.CASCADE)
    price = models.PositiveIntegerField()

    class Goal:
        managed = True
        db_table = 'store\".\"sales'
    
    def __str__(self):
        return self.sale_id