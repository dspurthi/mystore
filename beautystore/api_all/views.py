from django.shortcuts import render
from django.core.exceptions import MultipleObjectsReturned
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse,JsonResponse
from django.db.models import Q
from rest_framework import serializers
from rest_framework.parsers import JSONParser
from .models import Users,Sales,Products
from .serializers import UserSerializer,SalesSerializer,ProductsSerializer
import datetime
# import logging

# Get an instance of a logger
# logger = logging.getLogger(__name__)
# from django.utils import timzone 

# Create your views here.
@csrf_exempt
def Users_data(request):
    if request.method == 'GET':
        users_list = Users.objects.all()
        serializer = UserSerializer(users_list,many = True)
        return JsonResponse(serializer.data, safe= False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data,many = True)
        
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status = 201)
        return JsonResponse(serializer.errors,status=400)

@csrf_exempt
def Products_list(request):
    if request.method == 'GET':
        products_list = Products.objects.all()
        serializer = ProductsSerializer(products_list,many = True)
        return JsonResponse(serializer.data, safe= False)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ProductsSerializer(data=data)
        
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status = 201)
        return JsonResponse(serializer.errors,status=400)
        
@csrf_exempt

def Products_by_category(request,text):
    if request.method == 'GET':
        products_list = Products.objects.filter(category=text)
        serializer = ProductsSerializer(products_list,many=True)
        return JsonResponse({'answer': serializer.data})
    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors,status=400)

@csrf_exempt

def Products_by_brand(request,text):
    if request.method == 'GET':
        products_list = Products.objects.filter(brand=text)
        serializer = ProductsSerializer(products_list,many=True)
        return JsonResponse(serializer.data,safe=False)
    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors,status=400)
@csrf_exempt
def Sales_data(request):
    if request.method == 'GET':
        sales_list = Sales.objects.all()
        serializer = SalesSerializer(sales_list,many = True)
        return JsonResponse(serializer.data, safe= False)
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SalesSerializer(data=data)
        
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status = 201)
        return JsonResponse(serializer.errors,status=400)

# @csrf_exempt
# def electronics(request):
#     if request.method == 'GET':
#         products_list = Electronics.objects.all()
#         for product  in products_list:
#             # print(product)
#             # print(product.price)
#             disc_price = (product.price - (product.price*0.3))
#             # print(product.price)
#             serializer = ElectronicsSerializer(products_list,many=True)
#             # return render(request)
#         return JsonResponse(serializer.data, safe=False)

#         # serializer = ElectronicsSerializer(products_list,many = True)
#         # return JsonResponse(serializer.data, safe= False)
#     elif request.method == 'POST':
#         data = JSONParser().parse(request)
#         print(data)
#         serializer = ElectronicsSerializer(data=data)
        
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data,safe =False,status = 201)
#         return JsonResponse(serializer.errors,status=400)
#     elif request.method == 'DELETE':
#         Electronics.objects.all().delete()
#         return HttpResponse(status = 204)


# # @csrf_exempt

# # def Electronics_by_category(request,text):
# #     if request.method == 'GET':
# #         products_list = Electronics.objects.filter(category=text)
# #         serializer = ElectronicsSerializer(products_list,many=True)
# #         return JsonResponse({'answer': serializer.data})
# #     elif request.method == 'PUT':
# #         data = JSONParser().parse(request)
# #         serializer = ElectronicsSerializer(data=data)
# #         if serializer.is_valid():
# #             serializer.save()
# #             return JsonResponse(serializer.data)
# #         return JsonResponse(serializer.errors,status=400)

# # @csrf_exempt

# def Electronics_by_price_range(request,number):
#     if request.method == 'GET':
#         products_list = Electronics.objects.filter(price__lt = number)
#         serializer = ElectronicsSerializer(products_list,many=True)
#         return JsonResponse(serializer.data, safe=False)
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         serializer = ElectronicsSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors,status=400)

# @csrf_exempt
# def Electronics_by_price_range(request,number):
#     if request.method == 'GET':
#         x  = datetime.date.today()
#         # print(x)
#         if str(x) == "2021-10-22":
#             # print("spurthi")
#             products_list = Electronics.objects.all()
#             for product  in products_list:
#                 print(product)
#                 print(product.price)
#                 product.price = (product.price - (product.price*0.3))
#                 print(product.price)
#                 serializer = ElectronicsSerializer(products_list,many=True)
#             # return render(request)
#             return JsonResponse(serializer.data, safe=False)
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         serializer = ElectronicsSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors,status=400)

