from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import Users,Sales,Products

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ['user_id','first_name','last_name','email','phone','address']

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['product_id','product_name','brand','category','price','colour']

class SalesSerializer(serializers.ModelSerializer):
    # user = UserSerializer(many=False)
    class Meta:
        model = Sales
        fields = ['sale_id','user','product','price']
