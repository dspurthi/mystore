from django.urls import path
from .views import Sales_data,Products_by_category,Products_by_brand,Users_data,Products_list
urlpatterns = [
    path('products/',Products_list),
    path('products/<str:text>/',Products_by_category),
    path('products/brand/<str:text>/',Products_by_brand),
    path('users/',Users_data),
    path('sales/',Sales_data)
]