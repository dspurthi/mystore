from django.contrib import admin
from .models import Products, Sales,Users
from django import forms

# Register your models here.
admin.site.register(Products)
admin.site.register(Users)
admin.site.register(Sales)




# class ElectronicsForm(forms.ModelForm):

#     disc_price = forms.IntegerField()

#     def save(self, commit=True):
#         disc_price = self.cleaned_data.get('disc_price', None)
        
#         return super(ElectronicsForm, self).save(commit=commit)

#     class Meta:
#         model = Electronics
#         exclude = ()

# class ElectronicsAdmin(admin.ModelAdmin):

#     form = ElectronicsForm
#     fields = '__all__'